  <div class="footer-elements">
    <div class="logo-footer">
      <a class="logo" href="{$urls.base_url}" title="{$shop.name}">
        <img src="{$shop.logo}" alt="{$shop.name}">
      </a>
      {* DISPLAY SOCIAL ICONS AND LINKS *}
      {block name='hook_footer_after'}
        {hook h='displayFooterAfter'}
      {/block}
    </div>
    {foreach $linkBlocks as $linkBlock}
      <div class="links-container">
        <h3>{$linkBlock.title}</h3>
        <ul>
          {foreach $linkBlock.links as $link}
            <li>
              <a
                  id="{$link.id}-{$linkBlock.id}"
                  class="{$link.class}"
                  href="{$link.url}"
                  title="{$link.description}"
                  {if !empty($link.target)} target="{$link.target}" {/if}
              >
                {$link.title}
              </a>
            </li>
          {/foreach}
        </ul>
      </div>
    {/foreach}
  </div>
