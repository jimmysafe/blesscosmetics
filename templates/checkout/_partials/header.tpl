{* 
{block name='header_banner'}
  <div class="header-banner">
    {hook h='displayBanner'}
  </div>
{/block} *}

{block name='header_nav'}
  <div class="sidenav-overlay"></div>
  <div class="header-nav">
    <a href="#" id="bottoneMenu"><i class="small material-icons">menu</i></a>
    {block name='header_logo'}
      <div class="logo-cont">
        <a class="logo" href="{$urls.base_url}" title="{$shop.name}">
          <img src="{$shop.logo}" alt="{$shop.name}">
        </a>
      </div>
    {/block}
    <div class="nav-info">
      {hook h='displayNav2'}
    </div>
  </div>
{/block}

{block name='header_top'}
  {* <nav class="header-top">
    {hook h='displayTop'}
  </nav> *}

  {hook h='displayNavFullWidth'}

{/block}




{* 
{block name='header'}
  {block name='header_nav'}
    <nav class="header-nav">
      <div class="container">
        <div class="row">
          <div class="col-md-6 hidden-sm-down" id="_desktop_logo">
            <a href="{$urls.base_url}">
              <img class="logo img-responsive" src="{$shop.logo}" alt="{$shop.name} {l s='logo' d='Shop.Theme.Global'}">
            </a>
          </div>
          <div class="col-md-6 text-xs-right hidden-sm-down">
            {hook h='displayNav1'}
          </div>
          <div class="hidden-md-up text-sm-center mobile">
            {hook h='displayNav2'}
            <div class="float-xs-left" id="menu-icon">
              <i class="material-icons">&#xE5D2;</i>
            </div>
            <div class="float-xs-right" id="_mobile_cart"></div>
            <div class="float-xs-right" id="_mobile_user_info"></div>
            <div class="top-logo" id="_mobile_logo"></div>
            <div class="clearfix"></div>
          </div>
        </div>
      </div>
    </nav>
  {/block}

  {block name='header_top'}
    <div class="header-top hidden-md-up">
      <div class="container">
         <div class="row">
          <div class="col-sm-12">
            <div class="row">
              {hook h='displayTop'}
              <div class="clearfix"></div>
            </div>
          </div>
        </div>
        <div id="mobile_top_menu_wrapper" class="row hidden-md-up" style="display:none;">
          <div class="js-top-menu mobile" id="_mobile_top_menu"></div>
          <div class="js-top-menu-bottom">
            <div id="_mobile_currency_selector"></div>
            <div id="_mobile_language_selector"></div>
            <div id="_mobile_contact_link"></div>
          </div>
        </div>
      </div>
    </div>
    {hook h='displayNavFullWidth'}
  {/block}
{/block} *}
