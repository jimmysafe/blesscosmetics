<div class="container">
    {block name='hook_footer_before'}
      {hook h='displayFooterBefore'}
    {/block}
</div>
{* <div class="container">
  
</div> *}
<div>
</div>
<div class="footer-container">
  <div class="container">
    <h3 style="text-align:center">join us on instagram</h3>
    <p style="text-align:center">@blesscosmeticsofficial</p>
    <div id="instafeed"></div>
      {block name='hook_footer'}
        {hook h='displayFooter'}
      {/block}
  </div>
  <p
  style="    
    color: #7a7a7a;
    position: absolute;
    bottom:2px;
    width: 100%;
    text-align:center;
    font-size: .875rem;"
  >© 2019 - Bless. Cosmetics - Via Monte Napoleone 8, 20121 Milano, Italy - P.IVA/C.F 10245660963</p>
</div>
