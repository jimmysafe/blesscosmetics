
{extends file='customer/page.tpl'}

{block name='page_title'}
  {if $editing}
    <h1 style="text-align: center;">aggiorna il tuo indirizzo</h1>
  {else}
    <h1 style="text-align: center;">nuovo indirizzo</h1>
  {/if}
{/block}

{block name='page_content'}
<div class="container"></div>
  <div class="address-form">
    {render template="customer/_partials/address-form.tpl" ui=$address_form}
  </div>
  </div>
{/block}
