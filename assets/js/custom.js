$(window).load(function(){
  setTimeout(() => {
    $('#loader').fadeOut();
  }, 500)
});
$(document).ready(function(){

  AOS.init();
  $('.rss_slider').slick({
    dots: false,
    infinite: true,
    speed: 300,
    slidesToShow: 4,
    slidesToScroll: 4,
    controls: false,
    arrows: false,
    autoplay: true,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3,
          infinite: true,
          dots: false,
          arrows: false,
        }
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2
        }
      }
      // You can unslick at a given breakpoint now by adding:
      // settings: "unslick"
      // instead of a settings object
    ]
  });

    $('#bottoneMenu').click(function(){
        $('#sidenavMenu').addClass('sidenav-open')
        $('.sidenav-overlay').addClass('overlay-open')
    })
    $('.sidenav-overlay').click(function(){
        $('#sidenavMenu').removeClass('sidenav-open')
        $('.sidenav-overlay').removeClass('overlay-open')
    })
    $('.continue.btn.btn-primary').addClass('mybtn')

});

//const token = '8138798493.0256b0b.844fc9ee745944fa819c6704cc0982df';


  const token = '8138798493.1677ed0.ec17c2a6e55647929558850039babe44';
  console.log(token)
  const userid = 8138798493; 
  const num_photos = 15;

  fetch(`https://api.instagram.com/v1/users/${userid}/media/recent?access_token=${token}&count=${num_photos}`).then(res => {
    return res.json();
  }).then(data => {
    data.data.map(x => {
      let img = x.images.low_resolution.url
      let url = x.link
      console.log(img)
      $("#instafeed").append(`<a href='${url}' target='_blank'><div><div class="slick-img" style="background-image: url('${img}')"></div></div></a>`)
    })
  }).then(data => {
    return $('#instafeed').slick({
    dots: false,
    infinite: false,
    arrows: false,
    speed: 300,
    slidesToShow: 4,
    slidesToScroll: 4,
    autoplay: true,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3,
          infinite: true,
          dots: false
        }
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3,
          dots: false
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2,
          dots: false
        }
      }
      // You can unslick at a given breakpoint now by adding:
      // settings: "unslick"
      // instead of a settings object
    ]
  });
  })